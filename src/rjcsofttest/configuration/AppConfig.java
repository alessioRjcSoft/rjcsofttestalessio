package rjcsofttest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.mysql.cj.jdbc.MysqlDataSource;

@Configuration
@ComponentScan(basePackages="rjcsofttest")
@EnableWebMvc
public class AppConfig {
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver bean =new InternalResourceViewResolver();
		bean.setViewClass(JstlView.class);
		bean.setPrefix("/WEB-INF/");
		bean.setSuffix(".jsp");
		return bean;
	}
	@Bean
	public MysqlDataSource getDataSource() {
		
		MysqlDataSource objData = new MysqlDataSource();
		objData.setURL('jdbc:mysql://localhost:3306/db_example');
	}

}
