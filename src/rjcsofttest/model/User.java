package rjcsofttest.model;

import java.time.LocalDate;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class User {
	
	@NotNull
	@Min(8)
	private Integer id;
	
	@NotNull
	private String name;
	
	@NotNull
	private String surname;
	
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dateBirth;
	
	public User(Integer id, String name, String surname, LocalDate dateBirth) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.dateBirth = dateBirth;
	}

	public User() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public LocalDate getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(LocalDate dateBirth) {
		this.dateBirth = dateBirth;
	}
	
	
}
