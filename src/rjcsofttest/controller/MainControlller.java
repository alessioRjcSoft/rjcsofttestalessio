package rjcsofttest.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import rjcsofttest.model.User;

@Controller
public class MainControlller {

	@RequestMapping("main")
	public String mainpage(Model model) {
		User user = new User();
		
		user.setName("gigi");
		user.setSurname("Latrottola");
		
		model.addAttribute("user", user);
		return "PaginaPrincipale";
	}
	
	@RequestMapping("main-list")
	public String userlist(Model disney) {
		
		Map<String, String> map = new HashMap<>();
	    map.put("a", "Pippo");
	    map.put("b", "Paperino");
	    map.put("c", "Topolino");
	    disney.addAttribute("personaggi", map);
		
		return "PersonaggiDisney";
	}
	
	@RequestMapping("sendlist")
	public String userlist2(Model disney) {
		
		Map<String, String> map = new HashMap<>();
	    map.put("a", "Pippo");
	    map.put("b", "Paperino");
	    map.put("c", "pluto");
	    disney.addAttribute("personaggi", map);
		
		return "PersonaggiDisney";
	}
}
