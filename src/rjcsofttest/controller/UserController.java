package rjcsofttest.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated; ////Serve per l'annotation Validated
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rjcsofttest.model.User;


@Controller
@ControllerAdvice
public class UserController {
	
    //private Map<Integer, User> userMap = new HashMap<>();
 
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String submit(@Validated @ModelAttribute("user") User user, BindingResult result,  ModelMap model) {
        
    	if (result.hasErrors()) {
            return "Errore";
        }
    	
    	model.addAttribute("user", user);
        
        model.containsAttribute("user");
 
        //userMap.put(user.getId(), user);
 	
        return "ConfermaUtente";
    }
 
    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("msg", "Benvenuto in rjc soft test!");
    }
}
