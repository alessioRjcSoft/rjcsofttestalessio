<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registrazone</title>
</head>
<body>
	<p>Inserisci i tuoi dati per registrarti al sito</p>

	<form:form method="POST" action="/rjcsofttest/addUser" modelAttribute="user">
		<form:label path="id">id</form:label>
	    <form:input path="id" />
	 
	
	    <form:label path="name">Nome</form:label>
	    <form:input path="name" />
	    
	    <form:label path="surname">Cognome</form:label>
	    <form:input path="surname" />
	     
	    <form:label path="dateBirth">Data di nascita</form:label>
	    <form:input path="dateBirth"/>
	     
	    <input type="submit" value="Submit" />
	</form:form>
</body>
</html>