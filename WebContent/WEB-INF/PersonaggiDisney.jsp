<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Personaggi Disney</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
	<form:form modelAttribute="personaggi" method="POST" action="sendlist">
		<table border="1">
			<tr>
			  <th>To: </th>
			  <td>${personaggi}</td>
			</tr>
		</table>
		<input type="submit" value="send Message">
	</form:form>  
</body>
</html>